/* Nur Sefferyna Binti Sefie@Seffe
   Date: 20 Jun 2021
   Filename: quiz.js */

function result(){
	
	let Q1 = document.forms ["quizForm"]["q1"].value; 
	let Q2 = document.forms ["quizForm"]["q2"].value;
	let Q3 = document.forms ["quizForm"]["q3"].value;
	let Q4 = document.forms ["quizForm"]["q4"].value;
	let Q5 = document.forms ["quizForm"]["q5"].value;
	let Q6 = document.forms ["quizForm"]["q6"].value;
	let Q7 = document.forms ["quizForm"]["q7"].value;
	let Q8 = document.forms ["quizForm"]["q8"].value;
	let Q9= document.forms ["quizForm"]["q9"].value;
	let Q10 = document.forms ["quizForm"]["q10"].value;
	
	var name = document.forms ["quizForm"]["name"].value;
	var score=0;

	if (Q1==""){
		alert("Oops!! Question 1 is required")
		return false;
	}

	if(document.getElementById('correct1').checked){
		score++;
	}

	if (Q2==""){
		alert("Oops!! Question 2 is required")
		return false;
	}

	if(document.getElementById('correct2').checked){
		score++;
	}

	if (Q3==""){
		alert("Oops!! Question 3 is required")
		return false;
	}

	if(document.getElementById('correct3').checked){
		score++;
	}

	if (Q4==""){
		alert("Oops!! Question 4 is required")
		return false;
	}

	if(document.getElementById('correct4').checked){
		score++;
	}

	if (Q5==""){
		alert("Oops!! Question 5 is required");
		return false;
	}

	if(document.getElementById('correct5').checked){
		score++;
	}

	if (Q6==""){
		alert("Oops!! Question 6 is required");
		return false;
	}

	if(document.getElementById('correct6').checked){
		score++;
	}

	if (Q7==""){
		alert("Oops!! Question 7 is required");
		return false;
	}

	if(document.getElementById('correct7').checked){
		score++;
	}

	if (Q8==""){
		alert("Oops!! Question 8 is required");
		return false;
	}

	if(document.getElementById('correct8').checked){
		score++;
	}

	if (Q9==""){
		alert("Oops!! Question 9 is required");
		return false;
	}

	if(document.getElementById('correct9').checked){
		score++;
	}

	if (Q10==""){
		alert("Oops!! Question 10 is required");
		return false;
	}

	if(document.getElementById('correct10').checked){
		score++;
	}

	if (name==""){
		alert("Name is required");
		return false;
	}

	if (score>=0 && score<=4){
		alert ("Keep trying, " + name + "! You answered " + score + " out of 10 correctly");
	}

	else if (score >=5 && score<=9){
		alert ("Way to go, "+ name + "! You got " + score + " out of 10 correct");
	}

	else{
		alert ("Congratulations " + name + "! You got " + score + " out of 10");
	}

}	